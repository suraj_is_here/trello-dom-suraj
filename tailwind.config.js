/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/*.{html,js}"],
  theme: {
    extend: {
      boxShadow:{
        'create-shadow' : '0px 8px 12px #091E4226, 0px 0px 1px #091E424F'
      },
      gridTemplateColumns:{
        '1fr-max':'1fr max-content'
      }
    },
  },
  plugins: [
    require('daisyui'),
  ],
}
      

